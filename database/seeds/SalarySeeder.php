<?php


use Faker\Factory;
use Phinx\Seed\AbstractSeed;

class SalarySeeder extends AbstractSeed
{

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $faker = Factory::create();
        $data = [];
        for( $i = 0; $i < 30; $i++){
            $data[] = [
                'user_id'      => $faker->unique()->numberBetween(1, 30),
                'amount'      => $faker->numberBetween(1000, 10000),
                'created_at'       => date('Y-m-d H:i:s'),
                'updated_at'       => date('Y-m-d H:i:s'),
            ];
        }

        $this->table('salary')->insert($data)->save();
    }
}
