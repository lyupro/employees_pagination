<?php

namespace app\controllers;

use app\models\Main;
use app\plugins\Template;

class MainController extends Template
{
    /**
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function index()
    {
        $getTwig = self::twig();
        return $getTwig->render("index.html", ["content" => "Hello World!"]);
    }
}