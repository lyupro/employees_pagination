<?php

namespace app\controllers;

require_once __MODELS__."User.php";

use app\models\User;
use app\plugins\Template;

class UsersController extends Template
{
    /**
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */

    public function index()
    {
        $model = new User();
        $array = $model->getUsersInfo();

        foreach ($array as $user) {
            $getList .= "<li>
                    $user[middle_name] $user[first_name] $user[last_name]</td>
                    ($user[name])
                    $user[amount]$
                    </li>";
        }

        $pages = $model->getUsersPageCounter();
        $getPages = ceil($pages['number'] / 10);
        for($i = 1; $i <= $getPages; $i++)
        {
            $link .= "<a href='/users/?page=$i'>$i</a>&nbsp";
        }

        $getTwig = self::twig();
        return $getTwig->render("users.html", ["list"=>$getList, "link"=>$link]);
    }
}

