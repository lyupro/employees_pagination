<?php

namespace app\plugins;

class Template
{
    public static function twig()
    {
        $loader = new \Twig\Loader\FilesystemLoader(__ROOT__.'/views/');
        $twig = new \Twig\Environment($loader);
        return $twig;
    }
}