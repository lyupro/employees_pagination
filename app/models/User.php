<?php

namespace app\models;

use app\config\dbConnection;

class User extends dbConnection
{

    /**
     * @return array
     */
    public static function getUsersInfo()
    {
        if( isset($_GET['page']))
        {
            $getNumber = $_GET['page'];
            $counter = $getNumber * 10 - 10;
        }else
        {
            $counter = 0;
        }

        $connection = self::connectDB();
        $query = "SELECT us.*, ps.name, sl.amount FROM positions AS ps 
                    LEFT JOIN users AS us ON ps.id=us.position_id 
                    LEFT JOIN salary AS sl ON us.id=sl.user_id
                    LIMIT {$counter}, 10";
        $prepare = $connection->prepare($query);
        $exec = $prepare->execute();
        if( $exec )
        {
            $result = $prepare->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        }
    }

    public function getUsersPageCounter()
    {
        $connection = self::connectDB();
        $query = "SELECT COUNT(*) as number FROM users";
        $prepare = $connection->prepare($query);
        $exec = $prepare->execute();
        if( $exec )
        {
            $result = $prepare->fetch();
            return $result;
        }

    }
}