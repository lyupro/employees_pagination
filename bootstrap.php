<?php

require_once $_SERVER['DOCUMENT_ROOT']."/app/config/MainConfig.php";

require_once __ROOT__."/vendor/autoload.php";
require_once __CONFIG__."DBConfig.php";

require_once __PLUGINS__."twigExtends.php";
require_once __CONFIG__."RoutesConfig.php";

require_once __APP__."router.php";

